#!/bin/bash

# Dennis Pham
# A shitty build script
# Make sure you have a makefile that creates a debug target and a release target.
# version 1.1.2

confirm_makefile(){
	if [ -e makefile ]
	then
		echo -e "Lambsauce \e[91mlocated\e[0m"
	else
		echo My disappointment is immeasurable and my day has been ruined.
		touch makefile
		exit
	fi
}

check_source(){
	if [ -d src ]
	then
		echo Source confirmed.
	else
		echo move everything to src.
		mkdir src/
		exit
	fi
}

prep(){
	echo preparing prebuild
	make clean
}

clear
confirm_makefile
check_source
prep

EXE=$(grep "EXE =" makefile | cut -d ' ' -f 3)
DEBUG=$(grep "D.*DIR =" makefile | cut -d ' ' -f 3)
RELEASE=$(grep "RE.*DIR =" makefile | cut -d ' ' -f 3)

if [ $# -gt 0 ]
then
	if [ $1 == '-d' ]
	then
		echo creating debug executable
		echo -e "\e[91moh boy here I go breaking shit again.\e[0m"
		make debug
		./$DEBUG/$EXE
	else
		echo unknown command.
	fi
else
	echo creating release executable
	make
	./$RELEASE/$EXE
fi
